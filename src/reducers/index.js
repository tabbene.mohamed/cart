import { combineReducers } from 'redux';
import * as type from '../types';

const initShopState = {
  numberCart: 0,
  Carts: [],
  products: [],
};

function manageShopState(state = initShopState, action) {
  switch (action.type) {
    case type.GET_ALL_PRODUCT:
      return {
        ...state,
        products: action.payload,
      };
    case type.GET_NUMBER_CART:
      return {
        ...state,
      };
    case type.ADD_PRODUCT_TO_CART:
      if (state.numberCart === 0) {
        let cart = {
          id: action.payload.id,
          quantity: 1,
          title: action.payload.title,
          image: action.payload.image,
          price: action.payload.price,
        };
        state.Carts.push(cart);
      } else {
        let check = false;
        state.Carts.map((item, key) => {
          if (item.id === action.payload.id) {
            state.Carts[key].quantity++;
            check = true;
          }
        });
        if (!check) {
          let _cart = {
            id: action.payload.id,
            quantity: 1,
            title: action.payload.title,
            image: action.payload.image,
            price: action.payload.price,
          };
          state.Carts.push(_cart);
        }
      }
      return {
        ...state,
        numberCart: state.numberCart + 1,
      };
    case type.INCREASE_QUANTITY:
      state.numberCart++;
      state.Carts[action.payload].quantity++;

      return {
        ...state,
      };
    case type.DECREASE_QUANTITY:
      if (state.Carts[action.payload].quantity > 1) {
        state.numberCart--;
        state.Carts[action.payload].quantity--;
      }

      return {
        ...state,
      };
    case type.DELETE_PRODUCT_CART:
      return {
        ...state,
        numberCart: state.numberCart - state.Carts[action.payload].quantity,
        Carts: state.Carts.filter((item) => {
          return item.id !== state.Carts[action.payload].id;
        }),
      };
    default:
      return state;
  }
}
const ShopApp = combineReducers({
  _manageShopState: manageShopState,
});
export default ShopApp;
