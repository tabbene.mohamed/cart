import React from 'react';
import { Link } from 'react-router-dom';
import './Footer.scss';

const Footer = () => {
  return (
    <footer>
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <nav className="navbar navbar-light d-flex justify-content-center">
              <ul className="nav d-flex flex-row justify-content-center">
                <li className="nav-item">
                  <Link to="/" className="nav-link">
                    Accueil
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/" className="nav-link">
                    Boutique
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/" className="nav-link">
                    A propos
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/" className="nav-link">
                    Aide
                  </Link>
                </li>
              </ul>
            </nav>
            <p className="text-center">© your company name</p>
          </div>
        </div>
      </div>
    </footer>
  );
};
export default Footer;
