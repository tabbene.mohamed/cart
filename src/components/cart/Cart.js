import React from 'react';
import { IncreaseQuantity, DecreaseQuantity } from '../../actions';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import DeleteProductFromCart from './DeleteProductFromCart';
import { TotalPrice } from '../../utils/Utils';
import './Cart.scss';

const Cart = () => {
  const dispatch = useDispatch();
  const items = useSelector((state) => state._manageShopState);
  let ListCart = [];
  let TotalCart = 0;
  Object.keys(items.Carts).forEach(function (item) {
    TotalCart += items.Carts[item].quantity * items.Carts[item].price;
    ListCart.push(items.Carts[item]);
  });

  return (
    <>
      <nav class="nav nav-pills flex-column flex-sm-row">
        <span class=""></span>
        <Link to="/carts" className="flex-sm-fill text-sm-center nav-link active">
          1. Panier
        </Link>
        <Link to="/carts" className="flex-sm-fill text-sm-center nav-link">
          2. Livraison
        </Link>
        <Link to="/carts" className="flex-sm-fill text-sm-center nav-link">
          3. Paiement
        </Link>
      </nav>
      {ListCart.length > 0 ? (
        <div className="row">
          <div className="col-md-12">
            <table className="table">
              <thead>
                <tr>
                  <th></th>
                  <th>Name</th>
                  <th>Image</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Total Price</th>
                </tr>
              </thead>
              <tbody>
                {ListCart.map((item, key) => {
                  return (
                    <tr key={key}>
                      <td>
                        <DeleteProductFromCart id={key} name={item.title} />
                      </td>
                      <td width="250px">{item.title}</td>
                      <td>
                        <img
                          src={item.image}
                          style={{ width: '100px', height: '80px' }}
                          alt={item.title}
                        />
                      </td>
                      <td>{item.price} €</td>
                      <td>
                        <button
                          className="btn btn-primary"
                          style={{ margin: '2px' }}
                          onClick={() => dispatch(DecreaseQuantity(key))}
                        >
                          -
                        </button>
                        <span className="btn btn-light">{item.quantity}</span>
                        <button
                          className="btn btn-primary"
                          style={{ margin: '2px' }}
                          onClick={() => dispatch(IncreaseQuantity(key))}
                        >
                          +
                        </button>
                      </td>
                      <td>{TotalPrice(item.price, item.quantity).toFixed(3)} €</td>
                    </tr>
                  );
                })}
                <tr>
                  <td colSpan="5">Total Carts</td>
                  <td>{Number(TotalCart).toFixed(3)} €</td>
                </tr>
                <tr>
                  <td colSpan="5">Total Carts TVA</td>
                  <td>{Number(TotalCart + TotalCart / 5).toFixed(3)} €</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      ) : (
        <div className="alert alert-danger" role="alert">
          Panier est vide ...!
        </div>
      )}
    </>
  );
};

export default Cart;
