import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { actFetchProductsRequest, AddProductToCart } from '../../actions';
import { useDispatch, useSelector } from 'react-redux';
import ReactStars from 'react-rating-stars-component';
import './Product.scss';

const Product = () => {
  const dispatch = useDispatch();
  const products = useSelector((state) => state._manageShopState.products);

  useEffect(() => {
    dispatch(actFetchProductsRequest());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return products && products.length > 0 ? (
    <div className="row" style={{ marginTop: '10px' }}>
      <div className="col-md-12">
        <div className="row">
          {products.map((item, index) => (
            <div key={index} className="col-md-4">
              <div className="card mb-3">
                <img
                  src={item.image}
                  className="img-resposive"
                  style={{ width: '100%', height: '250px' }}
                  alt={item.title}
                />
                <div className="card-body">
                  <h5 className="card-title">{item.title}</h5>
                  <h6 className="font-weight-bold">Category : {item.category}</h6>
                  <ReactStars
                    count={5}
                    size={16}
                    value={item.rating.rate}
                    edit={false}
                    activeColor="#ffd700"
                  />
                  <SmartText text={item.description} length={60} />
                  <br />
                  <button
                    className="btn btn-dark w-100 mt-3"
                    style={{ cursor: 'pointer' }}
                    onClick={() => dispatch(AddProductToCart(item))}
                  >
                    Add Cart
                  </button>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  ) : (
    <div className="row">
      <h2 className="d-flex justify-content-center">Loading...!</h2>
    </div>
  );
};

const SmartText = ({ text, length }) => {
  const [showLess, setShowLess] = React.useState(true);

  if (text.length < length) {
    return <p className="card-text">{text}</p>;
  }

  return (
    <>
      <p className="card-text">{showLess ? `${text.slice(0, length)}...` : text}</p>
      <span className="BtnSmartText" onClick={() => setShowLess(!showLess)}>
        &nbsp;View {showLess ? 'More' : 'Less'}
      </span>
    </>
  );
};

SmartText.propTypes = {
  text: PropTypes.string.isRequired,
  length: PropTypes.number.isRequired,
};

export default Product;
