import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Cart from './components/cart/Cart';
import Header from './components/header/Header';
import Product from './components/product/Product';
import Footer from './components/footer/Footer';

function App() {
  return (
    <Router>
      <Header />
      <div className="container">
        <Switch>
          <Route path="/" exact component={Product} />
          <Route path="/carts" exact component={Cart} />
        </Switch>
      </div>
      <Footer />
    </Router>
  );
}

export default App;
