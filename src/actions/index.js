import callApi from '../api';
import * as type from '../types';

export const actFetchProductsRequest = () => {
  return (dispatch) => {
    return callApi('products', 'GET', null).then((res) => {
      dispatch(GetAllProduct(res.data));
    });
  };
};

export const GetAllProduct = (payload) => ({
  type: type.GET_ALL_PRODUCT,
  payload,
});

export const AddProductToCart = (payload) => ({
  type: type.ADD_PRODUCT_TO_CART,
  payload,
});

export const UpdateCart = (payload) => ({
  type: type.UPDATE_CART,
  payload,
});
export const DeleteProductCart = (payload) => ({
  type: type.DELETE_PRODUCT_CART,
  payload,
});

export const IncreaseQuantity = (payload) => ({
  type: type.INCREASE_QUANTITY,
  payload,
});
export const DecreaseQuantity = (payload) => ({
  type: type.DECREASE_QUANTITY,
  payload,
});
