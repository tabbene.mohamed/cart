import { TotalPrice } from './Utils';

describe('Total', () => {
  it('calculer total 10 * 5 eqale a 50', () => {
    expect(TotalPrice(10, 5)).toEqual(50);
  });
  it('calculer total 200 * 10 eqale a 2000', () => {
    expect(TotalPrice(200, 10)).toEqual(50);
  });
});
